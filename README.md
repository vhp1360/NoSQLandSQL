<div dir=rtl>بنام خدا</div>

# NoSQL
 
مقلاتی در مورد پایگاه های داده ای حجیم و ارتباطی

# Kafka
## نام کاربری و کلمه عبور
جهت امنیتی کردن آن می توان از [این](https://codeforgeek.com/how-to-set-up-authentication-in-kafka-cluster/) استفاده کرد. همچنین [این](https://docs.confluent.io/platform/current/kafka/incremental-security-upgrade.html#adding-mtls-security-to-zk) راهنما

## اجرابا امکان JMX
می توان از [این](https://docs.confluent.io/platform/current/installation/docker/operations/monitoring.html#launching-kafka-and-zk-with-jmx-enabled) راهنما استفاده کرد
